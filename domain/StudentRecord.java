package hazi_3.domain;

public class StudentRecord {
    //példány változók
    private String name;
    private String address;
    private int age;
    private double mathGrade;
    private double englishGrade;
    private double scienceGrade;
    private double average;

    //osztályváltozó
    private static int studentCount;

    public StudentRecord() {
    }


    //publikus konstruktor
    public StudentRecord(String név) {
        this(név, 1, 1, 1);
        StudentRecord.studentCount++;
    }

    //publikus konstruktor a legbővebb
    public StudentRecord(
            String név,
            double mathGrade,
            double englishGrade,
            double scienceGrade) {
        StudentRecord.studentCount++;
        this.name = név;
        this.mathGrade = mathGrade;
        this.englishGrade = englishGrade;
        this.scienceGrade = scienceGrade;
    }

    //publikus konstruktor ez nem ad nevet
    public StudentRecord(
            double mathGrade,
            double englishGrade,
            double scienceGrade) {
        this(null, mathGrade, englishGrade, scienceGrade);
    }
    //a tanuló nevével tér vissza
    public String getName() {
        return name;
    }
    //beállítja a tanuló nevét
    private void setName(String name) {
        this.name = name;
    }
    public static int getStudentCount() {
        return studentCount;
    }
    public double getMathGrade() {
        return mathGrade;
    }
    public void setMathGrade(double mathGrade) {
        this.mathGrade = mathGrade;
    }
    public double getEnglishGrade() {
        return englishGrade;
    }
    public void setEnglishGrade(double englishGrade) {
        this.englishGrade = englishGrade;
    }
    public double getScienceGrade() {
        return scienceGrade;
    }
    public void setScienceGrade(double scienceGrade) {
        this.scienceGrade = scienceGrade;
    }
    public double getAverage() {
        average = (englishGrade + mathGrade + scienceGrade) / 3;
        return average;
    }
}
