package hazi_3.domain;

import hazi_3.school.StudentHelper;

public class TrainingRecord {
    private String name;
    private String idő;
    private int terem;
    private StudentRecord[] diakok = new StudentRecord[15];
    final String schoolName = "Soter";

    //példányosításhoz névvel
    public TrainingRecord(String name) {
        this.name = name;
        System.out.println("Létrejött a >>" + this.name + "<< nevű TrainingRecord a nevet váró konstrukktorral");
    }

    //példányosításhoz névvel és idővel
    public TrainingRecord(String name, String idő) {
        this.name = name;
        this.idő = idő;
        System.out.println("Létrejött a >>" + this.name + "<< nevű TrainingRecord a nevet és " +
                ">>" + this.idő + "<< időt váró konstrukktorral");
    }

    //példányosításhoz névvel, idővel és teremszámmal
    public TrainingRecord(String name, String idő, int terem) {
        this.name = name;
        this.idő = idő;
        this.terem = terem;
        System.out.println("Létrejött a >>" + this.name + "<< nevű TrainingRecord a nevet, " +
                ">>" + this.idő + "<< időt és teremszámot >>" + this.terem + "<< váró konstrukktorral");
    }

    //példányosításhoz névvel, idővel, teremszámmal és tanulók tömbjével
    public TrainingRecord(String name, String idő, int terem, StudentRecord[] diakok) {

        int diakszam = 0;
        for (int i = 0; i < diakok.length; i++) {


            if (diakok[i] != null) {
                diakszam++;
            }
        }
        if (diakszam < 15) {
            this.name = name;
            this.idő = idő;
            this.terem = terem;
            this.diakok = diakok;
            System.out.println("Létrejött a >>" + this.name + "<< nevű TrainingRecord a nevet, " +
                    ">>" + this.idő + "<< időt, teremszámot >>" + this.terem + "<< " +
                    "és diakok tömböt >>(" + diakszam + "fő)<< " + "váró konstrukktorral");
        } else {
            System.out.println("Túl sok a jelentkező");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdő() {
        return idő;
    }

    public void setIdő(String idő) {
        this.idő = idő;
    }

    public int getTerem() {
        return terem;
    }

    public void setTerem(int terem) {
        this.terem = terem;
    }

    public StudentRecord[] getDiakok() {
        return diakok;
    }

    public void setDiakok(StudentRecord[] diakok) {
        this.diakok = diakok;
    }


    public void addDiak(StudentRecord diak) {
        int diakszam = 0;
        for (int i = 0; i < diakok.length; i++) {
            if (diakok[i] != null) {
                diakszam++;
            }
        }
        System.out.println("A hozzáadás előtt a jelentkezők száma eredetileg: " + diakszam);
        if (diakszam < 15) {
          //  diakszam++; ez ide nem kell
            diakok[diakszam] = diak;
            System.out.println("A >>" + this.getName() + "<< nevű tréninghez hozzá lett adva: >>" + diak.getName() + "<<");
            System.out.println("A hozzáadás után a jelentkezők száma " + diak.getName() + "-val 1-el nőtt: " + diakszam);
        } else {
            System.out.println("Túl sok a jelentkező, nem fut le az addDiak metódus..");
        }
    }

    public void trainingInformation(){
        int diakszam = 0;
        for (int i = 0; i < diakok.length; i++) {
            if (diakok[i] != null) {
                diakszam++;
            }
        }

        System.out.println("A training neve: "+this.schoolName+this.getName()+"; A training ideje: "
                +this.getIdő()+"; A training terme: "+this.getTerem()
                +"; A trainingre járó diákok száma: "+diakszam);

    }

    public void odaJáróDiákokÁtlaga(){

        int diakszam = 0;
        for (int i = 0; i < this.diakok.length; i++) {
            if (this.diakok[i] != null) {
                diakszam++;
            }
        }
        double average = 0;
        for(int i=0; i<diakszam; i++){
            System.out.println(this.diakok[i].getAverage());
            average = average+ this.diakok[i].getAverage();
        }
        average=average/diakszam;
        System.out.println("A "+this.getName()+" tréningre járó diákok átlaga ("+diakszam+" fő): "+average);
    }


    public void odaJáróLegjobb(){

        int diakszam = 0;
        for (int i = 0; i < this.diakok.length; i++) {
            if (this.diakok[i] != null) {
                diakszam++;
            }
        }

        StudentRecord[] diakokTrimmed = new StudentRecord[diakszam];
        for (int i=0; i<diakszam; i++){
            diakokTrimmed[i]=this.diakok[i];
        }
        System.out.println("A "+this.getName()+" -ra járó legjobb diak: "+StudentHelper.aLegjobbAtlag(diakokTrimmed).getName());

    }

}