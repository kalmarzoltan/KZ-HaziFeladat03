package hazi_3.school;


import hazi_3.domain.StudentRecord;

public class StudentHelper {

    public static void ketDiakNeve(String nev1, String nev2) {
        System.out.println(nev1 + "+" + nev2);
    }

    public static void ketDiakNeve(String nev1, String nev2, String nev3) {
        System.out.println(nev1 + "+" + nev2 + "+" + nev3);
    }

    public static double matekAtlag(StudentRecord[] students) {
        double matekAtlag = 0;

        for (int i = 0; i < students.length; i++) {
            matekAtlag = matekAtlag+students[i].getMathGrade();

        }
        matekAtlag = matekAtlag/(students.length);
        return matekAtlag;
    }

    public static double mindenAtlag(StudentRecord[] students) {
        double mindenAtlag = 0;

        for (int i = 0; i < students.length; i++) {
            mindenAtlag = mindenAtlag + students[i].getAverage();
        }
        mindenAtlag = mindenAtlag/students.length;
        return mindenAtlag;

    }
    public static StudentRecord aLegjobbAtlag(StudentRecord[] students){
        StudentRecord legjobbDiak = students[0];

        for (int i = 1; i < students.length; i++) {
            if(legjobbDiak.getAverage()< students[i].getAverage()){

                legjobbDiak = students[i];
            }

        }
        System.out.println("A legjobb diak: "+legjobbDiak.getName());
        return legjobbDiak;


    }


}
