package hazi_3.school;

import hazi_3.domain.StudentRecord;
import hazi_3.domain.TrainingRecord;

public class Main {
    public static void main(String[] args) {
        StudentRecord anna = new StudentRecord("Anna");
        anna.setMathGrade(5);
        anna.setEnglishGrade(3);
        anna.setScienceGrade(4);
        System.out.println("Anna átlaga: " + anna.getAverage());
        StudentRecord niki = new StudentRecord("Niki");
        niki.setMathGrade(5);
        niki.setEnglishGrade(5);
        niki.setScienceGrade(4);
        System.out.println("Niki átlaga: " + niki.getAverage());
        StudentRecord reni = new StudentRecord("Reni");
        reni.setMathGrade(4);
        reni.setEnglishGrade(2);
        reni.setScienceGrade(4);
        System.out.println("Reni átlaga: " + reni.getAverage());
        // anna.setName("Anna");
        System.out.println(anna.getName());
        // anna.setName("Csaba"); -> ez itt már nem működik mert a setter
        // metódus privatra lett állítva
        System.out.println("Studencount is: " + StudentRecord.getStudentCount());
        StudentHelper.ketDiakNeve(niki.getName(), reni.getName());
        StudentHelper.ketDiakNeve(niki.getName(), reni.getName(), anna.getName());
        StudentRecord béla = new StudentRecord("Béla", 2, 3, 2);

        System.out.println("Studencount is: " + StudentRecord.getStudentCount());
        StudentRecord sanya = new StudentRecord("Sanya", 1, 1, 1);
        System.out.println("Sanya átlaga alapból fa: " + sanya.getAverage());
        System.out.println("Studencount is: " + StudentRecord.getStudentCount());

        StudentRecord noName = new StudentRecord(3, 1, 2);
        System.out.println("A nonames konstruktorral kreált diak neve: "+noName.getName());


        StudentRecord[] diakok = {niki, sanya, reni, anna};
        StudentRecord[] diakok2 = {niki, sanya, reni, anna,niki, sanya, reni, anna,niki, sanya, reni,
                niki, sanya, reni, anna,niki};

        System.out.println("A matekjegyek átlaga: "+StudentHelper.matekAtlag(diakok));
        System.out.println("Az összes tárgy átlaga: "+StudentHelper.mindenAtlag(diakok));

        StudentRecord legjobb = StudentHelper.aLegjobbAtlag(diakok);
        System.out.println("A legjob diak: "+legjobb.getName());
        System.out.println("---------------------------------------------------------");



        StudentRecord[] diakok3 = new StudentRecord[15];
        diakok3[0]=reni;
        diakok3[1]=anna;
        diakok3[2]=sanya;


        TrainingRecord matek = new TrainingRecord("MATEK");
        TrainingRecord töri = new TrainingRecord("TÖRI", "8:00");
        TrainingRecord nyelvtan = new TrainingRecord("NYELVTAN", "9:00",2);

        TrainingRecord pumped = new TrainingRecord("PUMPED", "12:00",4,diakok2);


        TrainingRecord java = new TrainingRecord("JAVA", "10:00",3,diakok3);

        java.odaJáróDiákokÁtlaga();
        java.odaJáróLegjobb();
        java.addDiak(niki);
        java.trainingInformation();












    }

}