Készítsen egy TrainingRecord osztályt.

Adattagja legyen a tréning neve, a tréning ideje, teremszáma és az ott tanuló diákok listája(tömbje).
Lehessen példányosítani az osztályt a
-        tréning nevével,
-        a tréning nevével és idejével,
-        tréning nevével és idejével és teremszámával,
-        tréning nevével és idejével és teremszámával és tanulók tömbjével
A tréningre maximum 15 ember járhat. (setter / constructorban ellenőrizd, hogy 15nél ne nagyobb tömböt állítsanak be)

Mindegyik adattagot lehessen később módosítani.

Legyen metódusa, amivel új diákot adhatunk hozzá a kurzushoz, ha a kurzus betelt, akkor írjon ki hibaüzenetet a kozzolra.

- Egészítse ki az osztályt az iskola nevével. Iskola minden esetben „Soter” legyen.
- Egészítse ki az osztály egy metódussal, ami kiírja az osztály adatait.
- Egészítse ki az osztályt egy metódussal, ami visszaadja az oda járó diákok átlagát.
- Egészítse ki az osztályt egy metódussal, ami visszaadja a legjobb átlagú tanulót.



1. Készítsen el egy Alkalmazott osztályt és egy Alkalmazott osztályt 
használó osztályt több lépcsõben.
a) Az Alkalmazott osztályban  
- van név és fizetés adattag (félnyílvánosak)
- van egy metódusa, amely egy paraméterként kapott értékkel megnöveli a 
fizetest.
- van egy metódusa, amely egy String-be összefûzi a nevet és a fizetést és
ezt adja vissza.
Az AlkHasznalo osztályban egy main metódus van, amelyben létrehozunk egy 
Alkalmazottat értéket adunk a tagjainak, kiírjuk az adatait, megemeljük a 
fizetését és újra kiírjuk az adatait.

b) Írjuk át az adattagokat private-ra és készítsünk setter metódusokat az 
adatok beállításához. Írjuk át ennek megfelelõen a AlkHasznalo osztályt.

c) Írjuk át úgy hogy legyen konstruktora az Alkalmazottnak és írjuk át az
AlkHasznalo-t is ennek megfelelõen.

d) Egészítsük ki az Alkalmazott osztályt a következõ metódusokkal
  - adjon vissza igazat (true), ha a fizetés a paraméterként megadott
határok közé esik és hamisat ha nem.
  - adja vissza a fizetendõ adó értéket, ha az adókulcs 16%.
  - adjon vissza igazat, ha a fizetes nagyobb, mint egy paraméterként megadott
másik Alkalmazott-é.
Egészítsük ki a AlkHasznalo osztályt is úgy, hogy minden funkciót 
kipróbálhassunk.
